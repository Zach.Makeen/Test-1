package utilities;

public class MatrixMethod {

	public static void main(String[] args) {
		int[][] base = new int[][] {{1, 2, 3}, {4, 5, 6}};
		int[][] updated = duplicate(base);
		for(int i = 0; i < updated.length; i++) {
			for(int j = 0; j < updated[0].length; j++) {
				System.out.println(updated[i][j]);
			}
		}
	}
	public static int[][] duplicate(int[][] base) {
		int[][] updated = new int[2][6];
		int k = 0;
		for(int i = 0; i < updated.length; i++) {
			for(int j = 0; j < updated[0].length; j++, k++) {
				updated[i][j] = base[i][k];
				if(k == 2) {
					k = -1;
				}
			}
		}
		return updated;
	}
}