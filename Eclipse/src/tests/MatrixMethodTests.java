package tests;

import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void test() {
		int[][] base = new int[][] {{1, 2, 3}, {4, 5, 6}};
		int[][] updated = MatrixMethod.duplicate(base);
		int[][] expected = new int[][] {{1, 2, 3, 1, 2, 3}, {4, 5, 6, 4, 5, 6}};
		assertArrayEquals(expected, updated);
	}
}