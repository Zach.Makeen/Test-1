package tests;

import vehicles.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void carConstructorTest() {
		try {
			Car versa = new Car(-7);
		}
		catch(Exception IllegalArgumentException) {
			System.out.println("Error!");
		}
	}
	
	@Test
	void getSpeedTest() {
		Car murano = new Car(6);
		assertEquals(6, murano.getSpeed());
	}
	
	@Test
	void getLocationTest() {
		Car ferrari = new Car(9);
		assertEquals(0, ferrari.getLocation());
		ferrari.moveRight();
		assertEquals(9, ferrari.getLocation());
		ferrari.moveLeft();
		assertEquals(0, ferrari.getLocation());
		
	}
	
	@Test
	void moveRightTest() {
		Car bugatti = new Car(12);
		bugatti.moveRight();
		assertEquals(12, bugatti.getLocation());
	}
	
	@Test
	void moveLeftTest() {
		Car wrangler = new Car(6);
		wrangler.moveLeft();
		assertEquals(-6, wrangler.getLocation());
	}
	
	@Test
	void accelerateTest() {
		Car tesla = new Car(4);
		tesla.accelerate();
		assertEquals(5, tesla.getSpeed());
	}
	
	@Test
	void decelerateTest() {
		Car ford = new Car(4);
		ford.decelerate();
		assertEquals(3, ford.getSpeed());
	}
}
